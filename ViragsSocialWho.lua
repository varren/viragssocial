--
-- Created by IntelliJ IDEA.
-- User: Peter
-- Date: 15.08.14
-- Time: 15:50
-- To change this template use File | Settings | File Templates.
--
local ViragsSocial = Apollo.GetAddon("ViragsSocial")
ViragsSocial.ktAllWhoRoster = {}
local karRaceToString =
{
    [GameLib.CodeEnumRace.Human] 	= Apollo.GetString("CRB_ExileHuman"),
    [GameLib.CodeEnumRace.Granok] 	= Apollo.GetString("RaceGranok"),
    [GameLib.CodeEnumRace.Aurin] 	= Apollo.GetString("RaceAurin"),
    [GameLib.CodeEnumRace.Draken] 	= Apollo.GetString("RaceDraken"),
    [GameLib.CodeEnumRace.Mechari] 	= Apollo.GetString("RaceMechari"),
    [GameLib.CodeEnumRace.Chua] 	= Apollo.GetString("RaceChua"),
    [GameLib.CodeEnumRace.Mordesh] 	= Apollo.GetString("CRB_Mordesh"),
}

function ViragsSocial:OnWhoResponse(arResponse, eWhoResult, strResponse)
    if eWhoResult == GameLib.CodeEnumWhoResult.OK or eWhoResult == GameLib.CodeEnumWhoResult.Partial then
        if arResponse == nil or #arResponse == 0 then
            --self:PRINT(Apollo.GetString("Who_NoResults"))
            return
        end
        self:DEBUG("OnWhoResponse " .. #arResponse)
        for _, tWho in ipairs(arResponse) do
            tWho.eClass = tWho.eClassId or ViragsSocial.UNDEFINED
            tWho.ePathType = tWho.ePlayerPathType or ViragsSocial.UNDEFINED
            tWho.nRank = ViragsSocial.UNDEFINED
            tWho.strNote = karRaceToString[tWho.eRaceId] or ""
            tWho.fLastOnline   = 0
            tWho.refreshTime = os.time()
            self.ktAllWhoRoster[tWho.strName] = tWho
        end

        self:SetNewRosterForGrid(arResponse, ViragsSocial.Who)
        self:UpdateGrid(false, false)
    elseif eWhoResult == GameLib.CodeEnumWhoResult.UnderCooldown then
        --self:PRINT(Apollo.GetString("Who_UnderCooldown"))
    end


    if not self:IsVisible() then
        self:OnViragsSocialOn()
        self:UpdateUIAfterTabSelection(ViragsSocial.Who)
    end

    self:SetNewRosterForGrid(self:GetRosterForGrid (ViragsSocial.Who), ViragsSocial.Who)

end

function ViragsSocial:ReloadWhoList()

end


function ViragsSocial:OnUpdateLocationFromWho( wndHandler, wndControl, eMouseButton )
    self:DEBUG("OnUpdateLocationFromWho " )
    if self.kCurrGuild then
        if self.kCurrGuild:GetType() == GuildLib.GuildType_Guild then
            self:OnWho(self.kCurrGuild:GetName())
        elseif self.kCurrGuild:GetType() == GuildLib.GuildType_Circle then

        end

    end
end

function ViragsSocial:UpdatePlayerLocationWithWho(player)
    -- if online check location
    if player.fLastOnline == 0 then
        --if have no player info or info is outdated (loaded 10+ secs ago)
        local playerInfo = self.ktAllWhoRoster[player.strName]
        if playerInfo and os.time() - playerInfo.refreshTime > 10  then return end
        self:OnWho(player.strName)
    end

end
function ViragsSocial:OnWhoButtonClick()
    self:OnWho()
    self:UpdateUIAfterTabSelection(ViragsSocial.Who)
end

function ViragsSocial:OnWho(searchText)
    if not searchText then searchText = "" end

    self:CleanWhoOutdatedData()

    local cancelString = Apollo.GetString(1)
    local cmd

    if     cancelString  == "Cancel"    then   cmd = "/who"
    elseif cancelString  == "Abbrechen" then   cmd = "/wer"
    elseif cancelString  == "Annuler"   then   cmd = "/qui"
    end

    if cmd then
        cmd = cmd .. " \"" .. searchText .."\""
        self:DEBUG("OnWho WHO WHO WHO " .. cmd)
        self:DEBUG("OnWho searchText", searchText)
        ChatSystemLib.Command(cmd)
    end

end

function ViragsSocial:LocationFromWho(sName)
    if self.ktAllWhoRoster[sName] then
        return self.ktAllWhoRoster[sName].strZone
    end
end

function ViragsSocial:CleanWhoOutdatedData()
    local currTime = os.time()
    for name,v in pairs(self.ktAllWhoRoster) do
        -- cleanup outdated records 200 + secs old
        if(currTime > v.refreshTime + 200 )then
            self.ktAllWhoRoster[name] = nil
        end
    end
end

function ViragsSocial:DrawWhoList(tRoster)

    if tRoster == nil then
        self:ReloadWhoList()
        return
    end
    self:DEBUG("UpdateGrid!!!!!!: DrawWhoList")
    self:DEBUG("DrawWhoList()", tRoster)

    self:PrepareRoster(tRoster, self:cmpFn())

    local currTab = self.kSelectedTab
    for key, tCurr in pairs(tRoster) do
        local nameIcon = self:RelationsIconForPlayer(tCurr.strName, currTab)
        self:AddRow(tCurr, tCurr.eClass, tCurr.ePathType, tCurr.nLevel, tCurr.strName, tCurr.fLastOnline, "", tCurr.strNote, nameIcon, nil, nil, nil, tCurr.accurateLocation)
    end

end
